# EICE

#### Introduction
EICE software

#### Software Architecture
1.gui.py

Interface for EICE software

2.BVAnalysis.py

Bond valence method analysis

3.bv_ewald.py
init for BVEW analysis

3.Ewald_3D.py

EwaldSummation for 3D boundary conditions

3.Ewald_2D.py

EwaldSummation for 2D boundary conditions

4.Electro_match.py

Building heterojunction and calculating electrostatic matching

5.ewald_cation.py

Calculating site energy for BVEW

6.read_structure.py

Structure data for gui

7.Structure.py

Structure data for BVAnalysis

#### Required packages

python     3.7.4 
ase        3.19.0 
pymatgen   2020.12.31 
pandas     1.3.1 
numpy      1.21.1 
PyQt5      5.15.6
numba      0.55.1

#### Instructions for use
Put the .py .dat file in the same directory
Run the gui.py file
