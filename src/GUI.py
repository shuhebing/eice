'''
author: Wei Shi at 2022
version: 2022.9.16
function: for EICE-GUI
'''
import sys
from PyQt5.QtWidgets import QComboBox,QFormLayout,QTabWidget,QMainWindow,QComboBox, QLineEdit, QTextEdit,QFileDialog,QLabel,QApplication, QWidget, QToolTip, QPushButton, QMessageBox, QAction, QTextEdit,QHBoxLayout, QVBoxLayout, QApplication
from PyQt5.QtGui import QIcon, QFont
from PyQt5.QtCore import QCoreApplication
from Read_structure import Read
from PyQt5 import QtCore
from Ewald_3D import EwaldSummation
from Ewald_2D import Ewald2DSummation
from Ewald_1D import Ewald1DSummation
from Electro_match import Matching
from BV_ewald import Bondvalence_ewald

class EICE(QMainWindow):
     
    def __init__(self):
        super().__init__() 
        self.initUI()
        self.tab2s = None
        self.tab3s = None
        self.tab4sub = None
        self.tab4film = None
        self.tab5s_name = None
        self.tab5s = None
        self._elec_match = None
        self._film = None
        self._substrate = None
        self._hetero = None
        self._em = None
        self._nnntoremove = None
        
        self._tab3_ewald = 'Ewald_3D'
        self._tab4_ewald = 'Ewald_2D'
        
    def initUI(self):
        self.setFixedSize(640, 500)
        self.tab1 = QWidget()
        self.tab2 = QWidget()
        self.tab3 = QWidget()
        self.tab4 = QWidget()
        self.tab5 = QWidget()

        self.tabwidget = QTabWidget(self)
        self.tabwidget.resize(640, 500)
        self.tabwidget.addTab(self.tab1, "Version Information")
        self.tabwidget.addTab(self.tab2, "Material Information")
        self.tabwidget.addTab(self.tab3, "Ewald Caculation")
        self.tabwidget.addTab(self.tab4, "Electrostatic Matching")
        self.tabwidget.addTab(self.tab5, "Ionic Transport Analysis")
        #self.tabwidget.addTab(self.tab5, "Help")

        self.tab1UI()
        self.tab2UI()
        self.tab3UI()
        self.tab4UI()
        self.tab5UI()
        #self.tab5UI()
        #self.c.setLayout(vbox)
        self.setWindowTitle('EICE')
        

    def tab1UI(self):
        layout = QFormLayout()
        layout.addRow('Software developer： Weishi', QLabel())
        layout.addRow('Development date： 2022', QLabel())
        layout.addRow('version number：    V2.0', QLabel())
        self.tab1.setLayout(layout)

    def tab2UI(self):
        self.choose_materials = QLabel('Choose material',self)

        self.tab2_materials = QComboBox(self)
        #self.tab2_materials.addItem('Ubuntu')
        self.tab2_materials.activated[str].connect(self.tab2_materials_onActivated)


        self.addButton = QPushButton("Add")
        self.addButton.clicked.connect(self.read_structure)

        self.delButton = QPushButton("Del")

        self.saveButton = QPushButton("Save")

        self.quitButton = QPushButton("Quit")
        self.quitButton.clicked.connect(QCoreApplication.instance().quit)
 
        fun_box = QHBoxLayout() 
        
        fun_box.addWidget(self.addButton)
        fun_box.addWidget(self.delButton)
        fun_box.addWidget(self.saveButton)
        fun_box.addWidget(self.quitButton)

        self.materials_name = QLabel('Material    :', self)
        self.materials_name_text = QLineEdit()
        self.materials_name_text.setFrame(False)
        
        self.electrical_neutrality = QLabel('Electrically:', self)
        self.electrical_neutrality_text = QLineEdit()
        self.electrical_neutrality_text.setFrame(False)

        self.lattice_constants = QLabel('Lattice parameters:  ', self)
        self.lattice_constants_text = QTextEdit()

        self.reciprocal_lattice = QLabel('Reciprocal lattice vector:  ', self)
        self.reciprocal_lattice_text = QTextEdit()

        choose_box = QHBoxLayout()
        choose_box.addWidget(self.choose_materials)
        choose_box.addWidget(self.tab2_materials)

        lable_box = QHBoxLayout()
        lable_box.addWidget(self.materials_name)
        lable_box.addWidget(self.materials_name_text)
        lable_box.addWidget(self.electrical_neutrality)
        lable_box.addWidget(self.electrical_neutrality_text)

        constant_box = QHBoxLayout()
        constant_box.addWidget(self.lattice_constants)
        constant_box.addWidget(self.reciprocal_lattice)

        test_box = QHBoxLayout()
        test_box.addWidget(self.lattice_constants_text)
        test_box.addWidget(self.reciprocal_lattice_text)

        vbox = QVBoxLayout()
        vbox.addLayout(choose_box)
        vbox.addStretch(1)
        vbox.addLayout(lable_box)
        vbox.addStretch(1)
        vbox.addLayout(constant_box)
        vbox.addLayout(test_box)
        vbox.addStretch(1)
        vbox.addLayout(fun_box)
        
        self.tab2.setLayout(vbox)

    def tab3UI(self):
        self.choose_materials = QLabel('Choose material',self)

        self.tab3_materials = QComboBox(self)
        self.tab3_materials.activated[str].connect(self.tab3_materials_onActivated)

        self.choose_cal = QLabel('Choose Ewald method',self)
        self.cal = QComboBox(self)
        self.cal.addItem('Ewald_3D')
        self.cal.addItem('Ewald_2D')
        self.cal.addItem('Ewald_1D')
        self.cal.addItem('Ewald3DC_spherical_boundary')
        self.cal.addItem('Ewald3DC_planar_boundary')
        self.cal.activated[str].connect(self.tab3_cal_onActivated)

        self.total_energy_button  = QPushButton('Total electrostatic energy',self)
        self.total_energy_button.clicked.connect(self.calc_tot_energy)
        self.total_energy_button_text = QLineEdit()

        self.madelung_constant_button = QPushButton('Madelung constant',self)
        self.madelung_constant_button.clicked.connect(self.calc_madelung_constant)
        self.madelung_constant_button_text = QTextEdit()

        self.madelung_potential_button = QPushButton('Madelung potential for particles',self)
        self.madelung_potential_button.clicked.connect(self.calc_madelung_potential)
        self.madelung_potential_button_text = QTextEdit()
        
        self.force_button = QPushButton('Force on particles',self)
        self.force_button.clicked.connect(self.calc_force)
        self.force_button_text = QTextEdit()

        self.gap_potential = QLabel('Madelung potential for interstitial sites',self)
        self.gap_coordinates = QLabel('Input fractional coordinates',self)
        self.gap_coordinates_lineEdit = QLineEdit(self)
        #self.gap_coordinates_lineEdit.textChanged[str].connect(self.onChanged)

        
        self.gap_result = QPushButton('Madelung potential',self)
        self.gap_result.clicked.connect(self.calc_gap_potential)
        self.gap_lineEdit = QLineEdit(self)

        fun1_box = QHBoxLayout() 
        fun1_box.addWidget(self.choose_materials)
        fun1_box.addWidget(self.choose_cal)

        fun2_box = QHBoxLayout()
        fun2_box.addWidget(self.tab3_materials)
        fun2_box.addWidget(self.cal)

        fun3_box = QHBoxLayout()
        fun3_box.addWidget(self.total_energy_button)
        
        fun4_box = QHBoxLayout()
        fun4_box.addWidget(self.total_energy_button_text)
        
        fun3_new_box = QHBoxLayout()
        fun3_new_box.addWidget(self.madelung_constant_button)

        fun4_new_box = QHBoxLayout()
        fun4_new_box.addWidget(self.madelung_constant_button_text)

        fun5_box = QHBoxLayout()
        fun5_box.addWidget(self.madelung_potential_button)
        fun5_box.addWidget(self.force_button)

        fun6_box = QHBoxLayout()
        fun6_box.addWidget(self.madelung_potential_button_text)
        fun6_box.addWidget(self.force_button_text)

        fun7_box = QHBoxLayout()
        fun7_box.addWidget(self.gap_potential)

        fun8_box = QHBoxLayout()
        fun8_box.addWidget(self.gap_coordinates)
        fun8_box.addWidget(self.gap_coordinates_lineEdit)
        fun8_box.addWidget(self.gap_result)
        fun8_box.addWidget(self.gap_lineEdit)

        vbox = QVBoxLayout() 
        vbox.addLayout(fun1_box)
        vbox.addLayout(fun2_box)
        vbox.addStretch(1)
        vbox.addLayout(fun3_box)
        vbox.addLayout(fun4_box)
        vbox.addLayout(fun3_new_box)
        vbox.addLayout(fun4_new_box)
        vbox.addStretch(1)
        vbox.addLayout(fun5_box)
        vbox.addLayout(fun6_box)
        vbox.addLayout(fun7_box)
        vbox.addLayout(fun8_box)
        
        self.tab3.setLayout(vbox)   

    def tab4UI(self):
        self.choose_substrate = QLabel('substrate',self)
        self.tab4_substrate = QComboBox(self)
        self.tab4_substrate.activated[str].connect(self.tab4_substrate_onActivated)
        #self.materials.activated[str].connect(self.onActivated)
        self.choose_film = QLabel('overlayer',self)
        self.tab4_film = QComboBox(self)
        self.tab4_film.activated[str].connect(self.tab4_film_onActivated)

        self.tab4_choose_cal = QLabel('Choose Ewald method',self)
        self.tab4_cal = QComboBox(self)
        self.tab4_cal.addItem('Ewald_2D')
        self.tab4_cal.addItem('Ewald_3D')
        self.tab4_cal.activated[str].connect(self.tab4_cal_onActivated)
 
        self.tab4_sub_miller_index = QLabel('substrate_miller_index ',self)
        self.sub_miller_index_lineEdit = QLineEdit(self)

        self.tab4_film_miller_index = QLabel('overlayer_miller_index ',self)
        self.film_miller_index_lineEdit = QLineEdit(self)

        self.tab4_sub_atomic_layers = QLabel('substrate_atomic_layers',self)
        self.sub_atomic_layers_lineEdit = QLineEdit(self)

        self.tab4_film_atomic_layers = QLabel('overlayer_atomic_layers',self)
        self.film_atomic_layers_lineEdit = QLineEdit(self)
        
        self.tab4_interface_spacing = QLabel('Interface_spacing',self)
        self.interface_spacing_lineEdit = QLineEdit(self)
        self.interface_spacing_lineEdit.setMaximumWidth(425)
        
        self.build_hetero = QPushButton('Building_interface',self)
        self.build_hetero.clicked.connect(self.build_heterojunction)
        self.build_hetero_text = QLineEdit()
        self.build_hetero_text.setFrame(False)

        self.elec_match = QPushButton('Electrostatic_Matching',self)
        self.elec_match.clicked.connect(self.elec_matching)
        self.elec_match_text = QLineEdit()
        self.elec_match_text.setFrame(False)

        fun1_box = QHBoxLayout() 
        fun1_box.addWidget(self.choose_substrate)
        fun1_box.addWidget(self.choose_film)

        fun2_box = QHBoxLayout()
        fun2_box.addWidget(self.tab4_substrate)
        fun2_box.addWidget(self.tab4_film)

        fun3_box = QHBoxLayout()
        fun3_box.addWidget(self.tab4_choose_cal)

        fun4_box = QHBoxLayout()
        fun4_box.addWidget(self.tab4_cal)

        fun5_box = QHBoxLayout()
        fun5_box.addWidget(self.tab4_sub_miller_index)
        fun5_box.addWidget(self.sub_miller_index_lineEdit)
        fun5_box.addWidget(self.tab4_film_miller_index)
        fun5_box.addWidget(self.film_miller_index_lineEdit)
        

        fun6_box = QHBoxLayout()
        fun6_box.addWidget(self.tab4_sub_atomic_layers)
        fun6_box.addWidget(self.sub_atomic_layers_lineEdit)
        fun6_box.addWidget(self.tab4_film_atomic_layers)
        fun6_box.addWidget(self.film_atomic_layers_lineEdit)

        fun7_box = QHBoxLayout()
        fun7_box.addWidget(self.tab4_interface_spacing)
        fun7_box.addWidget(self.interface_spacing_lineEdit)

        fun8_box = QHBoxLayout()
        fun8_box.addWidget(self.build_hetero)

        fun9_box = QHBoxLayout()
        fun9_box.addWidget(self.build_hetero_text)

        fun10_box = QHBoxLayout()
        fun10_box.addWidget(self.elec_match)

        fun11_box = QHBoxLayout()
        fun11_box.addWidget(self.elec_match_text)

        vbox = QVBoxLayout() 
        vbox.addLayout(fun1_box)
        vbox.addLayout(fun2_box)
        vbox.addStretch(1)
        
        vbox.addLayout(fun3_box)
        vbox.addLayout(fun4_box)
        vbox.addStretch(1)
        
        vbox.addLayout(fun5_box)
        vbox.addLayout(fun6_box)
        vbox.addLayout(fun7_box)
        vbox.addStretch(1)
        
        vbox.addLayout(fun8_box)
        vbox.addStretch(1)

        vbox.addLayout(fun9_box)
        vbox.addStretch(1)
        
        vbox.addLayout(fun10_box)
        vbox.addStretch(1)

        vbox.addLayout(fun11_box)
        
        self.tab4.setLayout(vbox)    
        
    def tab5UI(self):
        self.tab5_choose_materials = QLabel('Choose material',self)

        #self.lbl = QLabel("Ubuntu", self)
        self.tab5_materials = QComboBox(self)
        self.tab5_materials.setMaximumWidth(800)
        self.tab5_materials.activated[str].connect(self.tab5_materials_onActivated)

        self.tab5_migration_ion = QLabel('migration ion',self)
        self.migration_ion_lineEdit = QLineEdit(self)
        self.migration_ion_lineEdit.setMaximumWidth(415)
        self.tab5_valence = QLabel('valence of migration ion',self)
        self.valence_lineEdit = QLineEdit(self)

        self.tab5_remove_points =  QLabel('points to be removed',self)
        self.tab5_rp = QComboBox(self)
        self.tab5_rp.addItem('Nearest neighbors')
        self.tab5_rp.addItem('Nearest neighbors and Next-nearest neighbors')
        self.tab5_rp.activated[str].connect(self.tab5_rp_onActivated)

        self.tab5_transport = QPushButton('BV_Ewald',self)
        self.tab5_transport.clicked.connect(self.transport_analysis)
        self.tab5_transport_text = QLineEdit()
        self.tab5_transport_text.setFrame(False)

        fun1_box = QHBoxLayout() 
        fun1_box.addWidget(self.tab5_choose_materials)
        fun1_box.addWidget(self.tab5_materials)

        fun2_box = QHBoxLayout()
        fun2_box.addWidget(self.tab5_migration_ion)
        fun2_box.addWidget(self.migration_ion_lineEdit)

        fun3_box = QHBoxLayout()
        fun3_box.addWidget(self.tab5_valence)
        fun3_box.addWidget(self.valence_lineEdit)

        fun4_box = QHBoxLayout()
        fun4_box.addWidget(self.tab5_remove_points)
        fun4_box.addWidget(self.tab5_rp)

        fun5_box = QHBoxLayout()
        fun5_box.addWidget(self.tab5_transport)

        fun6_box = QHBoxLayout()
        fun6_box.addWidget(self.tab5_transport_text)
        
        vbox = QVBoxLayout() 
        vbox.addLayout(fun1_box)
        vbox.addStretch(1)
        vbox.addLayout(fun2_box)
        vbox.addLayout(fun3_box)
        vbox.addStretch(1)
        vbox.addLayout(fun4_box)
        vbox.addStretch(1)
        vbox.addLayout(fun5_box)
        vbox.addLayout(fun6_box)

        self.tab5.setLayout(vbox)         
        
    def closeEvent(self, event):
        reply = QMessageBox.question(self, 'Message', 
            "Are you sure to quit?", QMessageBox.Yes |
            QMessageBox.No, QMessageBox.No)
 
        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

    def read_structure(self):
        structure_name = QFileDialog.getOpenFileName(self, 'add structure', '.')
        self.tab2_materials.addItem(structure_name[0].split('/')[-1])
        self.tab3_materials.addItem(structure_name[0].split('/')[-1])
        self.tab4_substrate.addItem(structure_name[0].split('/')[-1])
        self.tab4_film.addItem(structure_name[0].split('/')[-1])
        self.tab5_materials.addItem(structure_name[0].split('/')[-1])
        

    def materials_name_fun(self):
        self.textEdit.setText(data)
        

    def onChanged(self, text):
        self.coordinates.adjustSize()

    def tab2_materials_onActivated(self, text):
        self.tab2s = Read(text)
        self.materials_name_text.setText(self.tab2s.structure_name ())
        self.electrical_neutrality_text.setText(self.tab2s.electrical_neutrality())
        self.lattice_constants_text.setText (self.tab2s.structure_constants())
        self.reciprocal_lattice_text.setText (self.tab2s.structure_reciprocal_lattice())
        
    def tab3_materials_onActivated(self, text):
        self.tab3s = Read(text).return_structure()
    def tab3_cal_onActivated(self, text):
        self._tab3_ewald = text
        
    def tab4_substrate_onActivated(self, text):
        self.tab4sub = Read(text).return_structure()
    def tab4_film_onActivated(self, text):
        self.tab4film = Read(text).return_structure()
    def tab4_cal_onActivated(self,text):
        self._tab4_ewald = text
        
        
    def tab5_materials_onActivated(self, text):
        self.tab5s = Read(text).return_structure()
        self.tab5s_name = text
    def tab5_rp_onActivated(self,text):
        if text == 'Nearest neighbors and Next-nearest neighbors':
            self._nnntoremove = 1
        else:
            self._nnntoremove = 0

    def calc_tot_energy(self):
        if self._tab3_ewald == 'Ewald_3D':
            self.total_energy_button_text.setText(EwaldSummation(self.tab3s).total_energy())
        if self._tab3_ewald == 'Ewald_1D':
            self.total_energy_button_text.setText(Ewald1DSummation(self.tab3s).total_energy())
        elif self._tab3_ewald == 'Ewald3DC_spherical_boundary':
            self.total_energy_button_text.setText(EwaldSummation(self.tab3s,1,0).total_energy())
        elif self._tab3_ewald =='Ewald3DC_planar_boundary':
            self.total_energy_button_text.setText(EwaldSummation(self.tab3s,0,1).total_energy())
        elif self._tab3_ewald =='Ewald_2D':
            self.total_energy_button_text.setText(Ewald2DSummation(self.tab3s).total_energy())

    def calc_madelung_constant(self):
        if self._tab3_ewald == 'Ewald_3D':
            self.madelung_constant_button_text.setText(EwaldSummation(self.tab3s).get_madelung_constant())
        else:
            self.madelung_constant_button_text.setText('None')

    def calc_madelung_potential(self):
        potential = ''
        if self._tab3_ewald == 'Ewald_3D':
            for i in self.tab3s:
                potential = potential + i.symbol + str(i.index) +' '  + (EwaldSummation(self.tab3s).get_madelung_potential(i.position,scaled=0))
                potential = potential + '\n'
            self.madelung_potential_button_text.setText(potential)
        elif self._tab3_ewald == 'Ewald3DC_spherical_boundary':
            for i in self.tab3s:
                potential = potential + i.symbol + str(i.index) +' '  + (EwaldSummation(self.tab3s,1,0).get_madelung_potential(i.position,scaled=0))
                potential = potential + '\n'
            self.madelung_potential_button_text.setText(potential)
        elif self._tab3_ewald == 'Ewald3DC_planar_boundary':
            for i in self.tab3s:
                potential = potential + i.symbol + str(i.index) +' '  + (EwaldSummation(self.tab3s,0,1).get_madelung_potential(i.position,scaled=0))
                potential = potential + '\n'
            self.madelung_potential_button_text.setText(potential)
        elif self._tab3_ewald == 'Ewald_2D':
            for i in self.tab3s:
                potential = potential + i.symbol + str(i.index) +' '  + (Ewald2DSummation(self.tab3s).get_madelung_potential(i.position,scaled=0))
                potential = potential + '\n'
            self.madelung_potential_button_text.setText(potential)
        elif self._tab3_ewald == 'Ewald_1D':
            for i in self.tab3s:
                potential = potential + i.symbol + str(i.index) +' '  + (Ewald1DSummation(self.tab3s).get_madelung_potential(i.position,scaled=0))
                potential = potential + '\n'
            self.madelung_potential_button_text.setText(potential)

    def calc_force(self):
        if self._tab3_ewald == 'Ewald_3D':
            self.force_button_text.setText(EwaldSummation(self.tab3s).force())
        elif self._tab3_ewald == 'Ewald3DC_spherical_boundary':
            self.force_button_text.setText(EwaldSummation(self.tab3s,1,0).force())
        elif self._tab3_ewald =='Ewald3DC_planar_boundary':
            self.force_button_text.setText(EwaldSummation(self.tab3s,0,1).force())
        elif self._tab3_ewald =='Ewald_2D':
            self.force_button_text.setText(Ewald2DSummation(self.tab3s).force())
        elif self._tab3_ewald =='Ewald_1D':
            self.force_button_text.setText(Ewald1DSummation(self.tab3s).force())

    def calc_gap_potential(self):
        position = self.gap_coordinates_lineEdit.text()
        p = list(map(float,position.split(',')))
        if self._tab3_ewald == 'Ewald_3D':
            self.gap_lineEdit.setText(EwaldSummation(self.tab3s).get_madelung_potential(p))
        elif self._tab3_ewald == 'Ewald3DC_spherical_boundary':
            self.gap_lineEdit.setText(EwaldSummation(self.tab3s,1,0).get_madelung_potential(p))
        elif self._tab3_ewald == 'Ewald3DC_planar_boundary':
            self.gap_lineEdit.setText(EwaldSummation(self.tab3s,0,1).get_madelung_potential(p))
        elif self._tab3_ewald == 'Ewald_2D':
            self.gap_lineEdit.setText(Ewald2DSummation(self.tab3s).get_madelung_potential(p))
        elif self._tab3_ewald == 'Ewald_1D':
            self.gap_lineEdit.setText(Ewald1DSummation(self.tab3s).get_madelung_potential(p))

    def build_heterojunction(self):
     
        sub_mi = self.sub_miller_index_lineEdit.text()
        sub_mi = list(map(int,sub_mi.split(',')))
        film_mi = self.film_miller_index_lineEdit.text()
        film_mi = list(map(int,film_mi.split(',')))
        sp = float(self.interface_spacing_lineEdit.text())
        sub_al = int(self.sub_atomic_layers_lineEdit.text())
        film_al = int(self.film_atomic_layers_lineEdit.text())

        self._em = Matching (self.tab4sub, self.tab4film, sub_mi, film_mi, sp, sub_al, film_al, self._tab4_ewald)

        self._film, self._substrate, self._hetero = self._em.build_hetero()
        self.build_hetero_text.setText('The construction of the heterojunction is completed')

    def elec_matching(self):
        em_text = self._em.calc_em(self._film, self._substrate, self._hetero)
        self.elec_match_text.setText(em_text)

    def transport_analysis(self):  
        ion = self.migration_ion_lineEdit.text()
        valenceofion = int(self.valence_lineEdit.text())
        path = Bondvalence_ewald(self.tab5s_name, self.tab5s, ion, valenceofion, 0.2, self._nnntoremove)
        path.bv_calculation()
        self.tab5_transport_text.setText('BVEW analysis is completed')
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyle('Fusion')
    ex = EICE()
    ex.show()

    sys.exit(app.exec_())
