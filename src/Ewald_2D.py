'''
author: Wei Shi at 2022
version: 2022.9.18
function: for Ewald2D calculation
'''
import math
import numpy as np
from ase.io import read
from ase import Atoms
from numpy import pi,sqrt,log,exp,cos,sin
from scipy.special import erf
from math import erfc
from numba import jit

@jit(nopython=True)
def _sum_recip_total_energy(kmax, recip_cell, x, y, z_distance, charge1, charge2, distance, alpha):
    sum_addition = 0
    for b0 in range(-kmax,kmax+1):
        for b1 in range(-kmax,kmax+1):
            if b0==b1==0:
                continue
            h = b0*recip_cell[x]+b1*recip_cell[y]
            h2 = np.dot(h,h)
            d_h = sqrt(h2)                
            exponential1=exp(d_h*z_distance)*erfc((d_h/(2*alpha))+alpha*z_distance)
            exponential2=exp(-d_h*z_distance)*erfc((d_h/(2*alpha))-alpha*z_distance)
            temp1=charge1*charge2*cos(np.dot(h,distance))*(exponential1+exponential2)/d_h
            sum_addition += temp1
    return sum_addition

@jit(nopython=True)
def _sum_real_total_energy(rmax, real_cell, x, y, charge1, charge2, distance, alpha):
    sum_addition = 0
    for n0 in range(-rmax,rmax+1):
        for n1 in range(-rmax,rmax+1):
            a = n0*real_cell[x]+n1*real_cell[y]
            r = distance+a
            dsq = np.dot(r,r)
            d = sqrt(dsq)
            if dsq==0 and n0==n1==0:
                continue
            addition = charge1*charge2*erfc(alpha*d)/d
            sum_addition += addition
    return sum_addition

class Ewald2DSummation:
    CONV_FACT=14.399787146105348
    #CONV_FACT=14.39964547058623

    def __init__(self,structure,rmax=None,kmax=None,alpha=None,x=0,y=1,z=2):
        self._s=structure
        cell=structure.get_cell()
        self._square=np.linalg.norm(np.cross(cell[x],cell[y]))

        length_list=structure.get_cell_lengths_and_angles()
        cell_length=[length_list[0],length_list[1],length_list[2]]
        while 0 in cell_length:
            cell_length.remove(0)
        length=min(cell_length)
        self._x=x
        self._y=y
        self._z=z
        self._vol=structure.get_volume()
        self._alpha= alpha if alpha else (len(structure)* 1/sqrt(2)/(self._vol ** 2))**(1 / 6)*sqrt(pi) 
        self._rmax = rmax if rmax else math.ceil((sqrt(log(10 ** 12))/self._alpha)/length)
        self._kmax = kmax if kmax else int(2*sqrt(log(10 ** 12))*self._alpha)
        
        
    def get_madelung_potential(self,position=None,scaled=1):
        Sum=0
        Sum2=0
        switch=0
        cell=self._s.get_cell()
        Kcell=self._s.get_reciprocal_cell()*2*pi
        r_1 = np.array(position)
        
        if scaled:
            r1 = np.dot(r_1 , cell)
        else:
            r1 = r_1

        for b0 in range(-self._kmax,self._kmax+1):
            for b1 in range(-self._kmax,self._kmax+1):
                if b0==b1==0:
                    continue
                h=b0*Kcell[self._x]+b1*Kcell[self._y]
                h2 = np.dot(h,h)
                d_h = sqrt(h2)
                for i in self._s:
                    r2 = i.position.copy()
                    q2 = i.charge.copy()
                    zij=r1[self._z]-r2[self._z]
                    exponential1=exp(d_h*zij)*erfc((d_h/(2*self._alpha))+self._alpha*zij)
                    exponential2=exp(-d_h*zij)*erfc((d_h/(2*self._alpha))-self._alpha*zij)
                    temp=q2*cos(np.dot(h,r1-r2))*(exponential1+exponential2)/d_h
                    Sum +=temp

        for i in self._s:
                r2 = i.position.copy()
                q2 = i.charge.copy()
                zij=r1[self._z]-r2[self._z]
                temp=q2*(zij*erf(self._alpha*zij)+exp(-(self._alpha*zij)**2)/(self._alpha*sqrt(pi)))
                Sum2 +=temp
                        
        recip_potential1=Ewald2DSummation.CONV_FACT*pi*Sum/self._square 
        recip_potential2=-Ewald2DSummation.CONV_FACT*2*pi*Sum2/self._square 
        recip_potential=recip_potential1+recip_potential2 

        Sum = 0
        for i in self._s:
            for n0 in range(-self._rmax,self._rmax+1):
                for n1 in range(-self._rmax,self._rmax+1):
                        a=n0*cell[self._x]+n1*cell[self._y]
                        r=r1-i.position.copy()+a
                        dsq=np.dot(r,r)
                        d=sqrt(dsq)
                        if dsq<=10e-7:
                            index=i.index
                            switch=1
                            continue
                        else:
                            addition = i.charge.copy()*erfc((self._alpha)*d)/d
                            Sum += addition
                      
        real_potential = Sum*Ewald2DSummation.CONV_FACT
        
        if switch:
            self_potential = Ewald2DSummation.CONV_FACT*2*self._alpha*self._s[index].charge/sqrt(pi)
            potential=(recip_potential+real_potential-self_potential)
            #print('',"madelung potential2D:",potential)
            return str(potential)
        else:
            potential=(recip_potential+real_potential)
            #print('',"madelung potential2D:",potential)
            return str(potential)
   
    def total_energy(self):
        Sum=0
        Sum2=0
        cell=np.array(self._s.get_cell())
        Kcell=self._s.get_reciprocal_cell()*2*pi

        ######## long range ###################
        for i in self._s:
            r1 = i.position.copy()
            q1 = i.charge.copy()
            for j in self._s:
                q2 = j.charge.copy()
                r2 = j.position.copy()
                r1r2 = r1-r2
                zij = r1[self._z]-r2[self._z]
                addition1 = _sum_recip_total_energy(kmax=self._kmax, recip_cell=Kcell, x=self._x, y=self._y, z_distance=zij, charge1=q1, charge2=q2, distance=r1r2, alpha=self._alpha)
                Sum += addition1
                        
        for i in self._s:
            r1 = i.position.copy()
            q1 = i.charge.copy()
            for j in self._s:
                q2 = j.charge.copy()
                r2 = j.position.copy()
                zij=r1[self._z]-r2[self._z]
                temp=q1*q2*(zij*erf(self._alpha*zij)+exp(-(self._alpha*zij)**2)/(self._alpha*sqrt(pi)))
                Sum2 += temp
                
        recip_energy_1 = Ewald2DSummation.CONV_FACT*pi*Sum/(2*self._square) 
        recip_energy_2 = -Ewald2DSummation.CONV_FACT*pi*Sum2/self._square 
        recip_energy = recip_energy_1+recip_energy_2

        ######### short range ###################
        Sum = 0
        for i in self._s:
            r1 = i.position.copy()
            q1 = i.charge.copy()
            for j in self._s:
                q2 = j.charge.copy()
                r2 = j.position.copy()
                r1r2 = r1-r2
                addition1 = _sum_real_total_energy(rmax=self._rmax, real_cell=cell, x=self._x, y=self._y, charge1=q1, charge2=q2, distance=r1r2, alpha=self._alpha)
                Sum += addition1
        real_energy = Sum*Ewald2DSummation.CONV_FACT/2
        
        Sum=0
        for i in self._s:
            Sum +=i.charge*i.charge
        self_energy = ((-self._alpha)/sqrt(pi))*Sum*Ewald2DSummation.CONV_FACT
        energy=(recip_energy + real_energy + self_energy)
        return str(energy)

    def force(self):
        result = ''
        for ion in self._s:
            Sum_xy = 0
            Sum_z = 0
            r1 = ion.position
            Kcell=self._s.get_reciprocal_cell()*2*pi

            for i in self._s:
                r2 = i.position.copy()
                q2 = i.charge.copy()
                zij=r1[self._z]-r2[self._z]
                for b0 in range(-self._kmax,self._kmax+1):
                    for b1 in range(-self._kmax,self._kmax+1):
                            if b0==b1==0:
                                continue
                            else:
                                k=b0*Kcell[self._x]+b1*Kcell[self._y]
                                k2 = np.dot(k,k)
                                k1 = sqrt(k2)

                                structure_factor_xy= q2*(sin(np.dot(k,r1-r2)))*k
                                exponential1_xy=exp(k1*zij)*erfc((k1/(2*self._alpha))+self._alpha*zij)
                                exponential2_xy=exp(-k1*zij)*erfc((k1/(2*self._alpha))-self._alpha*zij)
                                addition =(exponential1_xy+exponential2_xy)*structure_factor_xy
                                Sum_xy +=addition            

                                structure_factor_z = q2*cos(np.dot(k,r1-r2))/k1
                                
                                exponential1_xy = exp(-k1*zij)*erfc((k1/(2*self._alpha))-self._alpha*zij)
                                exponential2_xy = -exp(k1*zij)*erfc((k1/(2*self._alpha))+self._alpha*zij)
                                
                                external = 2*self._alpha/sqrt(pi)

                                exp_z1 = exp(k1*zij-((k1/(2*self._alpha))+self._alpha*zij)**2)
                                exp_z2 = -exp(-k1*zij-((k1/(2*self._alpha))-self._alpha*zij)**2)
                                
                                addition = (exponential1_xy + exponential2_xy + external*(exp_z1+exp_z2))*structure_factor_z                              
                                Sum_z +=addition
                                
            recip_1_xy = Ewald2DSummation.CONV_FACT*2*pi*Sum_xy/self._square
            recip_1_z = Ewald2DSummation.CONV_FACT*pi*Sum_z/self._square
            
            Sum_z = 0
            for i in self._s:
                r2 = i.position.copy()
                q2 = i.charge.copy()
                zij=r1[self._z]-r2[self._z]
                addition = q2*erf(self._alpha*zij)
                Sum_z += addition
                                
            recip_0_z = Ewald2DSummation.CONV_FACT*2*pi*Sum_z/self._square

            Sum = 0
            cell=self._s.get_cell()
            for i in self._s:
                r2=i.position
                for n0 in range(-self._rmax,self._rmax+1):
                    for n1 in range(-self._rmax,self._rmax+1):
                        a=n0*cell[self._x]+n1*cell[self._x]
                        r=r1-r2+a
                        dsq=np.dot(r,r)
                        d=sqrt(dsq)
                        if dsq<=10e-7:
                            continue
                        else:
                            part1=(2*self._alpha/sqrt(pi))*exp(-(self._alpha*self._alpha)*dsq)/dsq
                            part2=erfc(self._alpha*d)/(d**3)
                            addition= i.charge*(part1+part2)*r
                            Sum+=addition
                            
            real= Sum*Ewald2DSummation.CONV_FACT  
            F=ion.charge*(recip_1_xy + np.array([0,0,recip_1_z]) + np.array([0,0,recip_0_z]) + real)
            result = result + ion.symbol + str(ion.index)+' ' + str(F)
            result = result + '\n'
        return(result)
