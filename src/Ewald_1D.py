import math
import numpy as np
from ase.io import read
from ase import Atoms
from numpy import pi,sqrt,log,exp,cos,sin
from scipy.special import erfc,erf

class Ewald1DSummation:
    CONV_FACT=14.399787146105348

    def __init__(self,structure,rmax=20,kmax=20,alpha=0.5,x=0,y=1,z=2):
        self._s=structure
        cell=structure.get_cell()
        self._kmax = kmax
        self._rmax = rmax
        self._alpha = alpha
        self._x=x
        self._y=y
        self._z=z
        
    def total_energy(self):
        
        Sum=0
        # setting unit cell
        axis=self._z       
        dx_list=[]
        dy_list=[]
        for i in self._s.positions:
            for j in self._s.positions:
                dx=abs(i[self._x]-j[self._x])
                dy=abs(i[self._y]-j[self._y])
                dx_list.append(dx)
                dy_list.append(dy)
        d=max(max(dx_list),max(dy_list)) 
        factor=0.00001
        lp=d/factor 
        lz=self._s.get_cell_lengths_and_angles()[axis] 
        volume=lp*lp*lz
        
        Kcell_a = np.array([2*pi/lp,0,0])
        Kcell_b = np.array([0,2*pi/lp,0])
        Kcell_c = np.array([0,0,2*pi/lz])

        # long-range term
        for i in self._s:
            r1 = i.position
            q1 = i.charge
            for j in self._s:
                r2 = j.position
                q2 = j.charge
                for b0 in range(self._kmax,self._kmax+1):
                    for b1 in range(self._kmax,self._kmax+1):
                        for b2 in range(self._kmax,self._kmax+1):
                            if b0==b1==b2==0:
                                continue
                            else:
                                k=b0*Kcell_a+b1*Kcell_b+b2*Kcell_c 
                                k2 = np.dot(k,k)
                                structure_factor= q1*q2*(np.cos(np.dot(k,r1-r2)))
                                exponential=np.exp(-k2/(4.0*self._alpha*self._alpha))/k2 
                                addition =exponential*structure_factor
                                Sum +=addition

        E_k = Ewald1DSummation.CONV_FACT*2*pi*Sum/volume

        # IB term
        Sum=0
        for i in self._s:
            q1 = i.charge
            for j in self._s:
                q2 = j.charge
                xij=(i.position[self._x]-j.position[self._x])**2
                yij=(i.position[self._y]-j.position[self._y])**2
                addition=q1*q2*(xij+yij)
                Sum +=addition
        E_ib = Ewald1DSummation.CONV_FACT*pi*Sum/(2*volume)

        # self term
        Sum=0
        for i in self._s:
            Sum +=i.charge*i.charge
        
        E_s = (-self._alpha/sqrt(pi))*Sum*Ewald1DSummation.CONV_FACT

        Sum = 0
        cell=np.array(self._s.get_cell())
        number = len(self._s)

        # short-range term
        for i in range(number):
            for j in range(number):
                for n2 in range(-self._rmax,self._rmax+1):
                    if i==j and n2==0:
                        continue
                    else:
                        a=n2*cell[axis]
                        r=self._s[i].position.copy()-self._s[j].position.copy()+a
                        dsq=np.dot(r,r)
                        d=sqrt(dsq)
                        addition = self._s[i].charge.copy()*self._s[j].charge.copy()*erfc(self._alpha*d)/(d)
                        Sum += addition
        E_r = Sum*Ewald1DSummation.CONV_FACT*1/2

        total_energy=E_k+E_ib+E_s+E_r
        return str(total_energy)

    def get_madelung_potential(self,position=None,scaled=1):
        Sum=0
        switch=0
        number = len(self._s)
        # setting unit cell
        axis=self._z       
        dx_list=[]
        dy_list=[]
        for i in self._s.positions:
            for j in self._s.positions:
                dx=abs(i[self._x]-j[self._x])
                dy=abs(i[self._y]-j[self._y])
                dx_list.append(dx)
                dy_list.append(dy)
        d=max(max(dx_list),max(dy_list)) 
        factor=1e-5
        lp=d/factor
        lz=self._s.get_cell_lengths_and_angles()[axis] 
        volume=lp*lp*lz
        
        Kcell_a = np.array([2*pi/lp,0,0])
        Kcell_b = np.array([0,2*pi/lp,0])
        Kcell_c = np.array([0,0,2*pi/lz])

        cell=np.array(self._s.get_cell())
        r_1 = np.array(position)
        if scaled:
            r1 = np.dot(r_1 , cell)
        else:
            r1 = r_1

        # long-range term
        for j in self._s:
            r2 = j.position
            q2 = j.charge
            for b0 in range(self._kmax,self._kmax+1):
                for b1 in range(self._kmax,self._kmax+1):
                    for b2 in range(self._kmax,self._kmax+1):
                        if b0==b1==b2==0:
                            continue
                        else:
                            k = b0*Kcell_a+b1*Kcell_b+b2*Kcell_c 
                            k2 = np.dot(k,k)
                            structure_factor= q2*(np.cos(np.dot(k,r1-r2)))
                            exponential=np.exp(-k2/(4.0*self._alpha*self._alpha))/k2
                            addition =exponential*structure_factor
                            Sum +=addition

        E_k = Ewald1DSummation.CONV_FACT*4*pi*Sum/volume

        # IB term
        Sum=0
        for j in self._s:
            q2 = j.charge
            xij=(r_1[self._x]-j.position[self._x])**2
            yij=(r_1[self._y]-j.position[self._y])**2
            addition=q2*(xij+yij)
            Sum +=addition
        E_ib = Ewald1DSummation.CONV_FACT*pi*Sum/(volume)
        
        Sum=0  
        # short-range term
        for j in self._s:
            for n0 in range(-self._rmax,self._rmax+1):
                a=n0*cell[self._z]
                r= r_1 -j.position.copy()+a
                dsq=np.dot(r,r)
                d=sqrt(dsq)
                if dsq<=10e-7:
                    index=j.index
                    switch=1
                    continue
                else:
                    addition = j.charge.copy()*erfc(self._alpha*d)/(d)
                    Sum += addition
        E_r = Sum*Ewald1DSummation.CONV_FACT

        # self term
        if switch:
            self_potential = Ewald1DSummation.CONV_FACT*2*self._alpha*self._s[index].charge/sqrt(pi)
            potential=(E_r + E_k + E_ib - self_potential)
            return str(potential)
        else:
            potential=(E_r + E_k + E_ib)
            return str(potential)

    def force(self):

        Sum=0
        # setting unit cell
        axis=self._z       
        dx_list=[]
        dy_list=[]
        for i in self._s.positions:
            for j in self._s.positions:
                dx=abs(i[self._x]-j[self._x])
                dy=abs(i[self._y]-j[self._y])
                dx_list.append(dx)
                dy_list.append(dy)
        d=max(max(dx_list),max(dy_list)) 
        factor=1e-5
        lp=d/factor 
        lz=self._s.get_cell_lengths_and_angles()[axis] 
        volume=lp*lp*lz
        
        Kcell_a = np.array([2*pi/lp,0,0])
        Kcell_b = np.array([0,2*pi/lp,0])
        Kcell_c = np.array([0,0,2*pi/lz])
        
        result = ''
        for ion in self._s:
            #long-range term
            Sum=0
            r1 = ion.position
            for i in self._s:
                r2 = i.position.copy()
                q2 = i.charge.copy()
                for b0 in range(-self._kmax,self._kmax+1):
                    for b1 in range(-self._kmax,self._kmax+1):
                        for b2 in range(-self._kmax,self._kmax+1):
                            if b0==b1==b2==0:
                                continue
                            else:
                                k = b0*Kcell_a+b1*Kcell_b+b2*Kcell_c 
                                k2 = np.dot(k,k) 
                                structure_factor= q2*(sin(np.dot(k,r1-r2)))
                                exponential=exp(-k2/(4.0*self._alpha*self._alpha))/k2 
                                addition =exponential*structure_factor*k
                                Sum +=addition            
            recip=Ewald1DSummation.CONV_FACT*4*pi*Sum/(volume)

            # short-range term
            Sum = 0
            cell=np.array(self._s.get_cell())
            for i in self._s:
                r2=i.position
                for n0 in range(-self._rmax,self._rmax+1):
                    a=n0*cell[self._z]
                    r=r1-r2+a
                    dsq=np.dot(r,r)
                    d=sqrt(dsq)
                    if dsq<=10e-7:
                        continue
                    else:
                        part1=(2*self._alpha/sqrt(pi))*exp(-(self._alpha*self._alpha)*dsq)/dsq
                        part2=erfc(self._alpha*d)/(d**3)
                        addition= i.charge*(part1+part2)*r
                        Sum+=addition
            real= Sum*Ewald1DSummation.CONV_FACT

            # IB-term
            Sum = 0
            r1 = ion.position
            for i in self._s:
                r2 = i.position
                xij=sqrt((r1[self._x]-i.position[self._x])**2)
                yij=sqrt((r1[self._y]-i.position[self._y])**2)
                addition = i.charge.copy()*(xij + yij)
                Sum += addition

            ib= Ewald1DSummation.CONV_FACT*2*ion.charge*pi*Sum/volume
            
            F=ion.charge*(recip+real-ib)
          
            result = result + ion.symbol + str(ion.index)+' ' + str(F)
            result = result + '\n'
        return(result)
